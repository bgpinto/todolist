package com.example.todolist

import android.app.Application
import com.example.domain.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class TodoApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TodoApplication)
            modules(domainModule)
        }
    }
}
