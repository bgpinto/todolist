package com.example.todolist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.models.TodoItemModel
import com.example.todolist.R


class TodoItemView(private val mLayout: View) : RecyclerView.ViewHolder(mLayout) {
    val mText: TextView = mLayout.findViewById(R.id.main_headline)
}

class TodoListItemAdapter :
    RecyclerView.Adapter<TodoItemView>(), Observer<List<TodoItemModel>> {

    private val mItems = arrayListOf<TodoItemModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TodoItemView(
            LayoutInflater.from(parent.context).inflate(R.layout.todo_item_layout, parent, false)
        )

    override fun getItemCount() = mItems.size


    override fun onBindViewHolder(holder: TodoItemView, position: Int) {
        holder.mText.text = mItems[position].title
    }

    override fun onChanged(newList: List<TodoItemModel>?) {
        newList?.let {
            mItems.clear()
            mItems.addAll(it.toMutableList())
            notifyDataSetChanged()
        }
    }
}
