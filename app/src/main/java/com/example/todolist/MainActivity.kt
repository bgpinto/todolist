package com.example.todolist

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.viewmodels.TodoItemListViewModel
import com.example.todolist.adapter.TodoListItemAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    val mTodoListViewModel: TodoItemListViewModel by viewModel()

    private val mTodoAdapter = TodoListItemAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mTodoListViewModel.items().observe(this, mTodoAdapter)

        todoItemListView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity) as RecyclerView.LayoutManager?
            adapter = mTodoAdapter
        }

        mTodoListViewModel.load()
    }
}
