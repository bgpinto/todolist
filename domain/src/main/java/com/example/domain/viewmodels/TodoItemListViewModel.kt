package com.example.domain.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.models.TodoItemModel
import com.example.domain.repository.TodoItemRepository
import kotlinx.coroutines.launch

class TodoItemListViewModel(private val mTodoItemRepository: TodoItemRepository) : ViewModel() {

    private val mTodoList: MutableLiveData<List<TodoItemModel>> = MutableLiveData()

    fun items(): LiveData<List<TodoItemModel>> = mTodoList

    fun load() = viewModelScope.launch {
        mTodoItemRepository.fetchAll({ list ->
            mTodoList.postValue(list)
        }
            , Throwable::printStackTrace
        )
    }

}
