package com.example.domain.models

data class TodoItemModel(
    val userId: Int,
    val id: Int,
    val title: String,
    val completed: Boolean
)
