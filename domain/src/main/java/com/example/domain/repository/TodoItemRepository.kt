package com.example.domain.repository

import com.example.domain.models.TodoItemModel
import com.example.domain.sources.DataSource


class TodoItemRepository(private val mSource: DataSource) {
    fun fetchAll(onSuccess: (List<TodoItemModel>) -> Unit, onError: (Throwable) -> Unit) {
        mSource.fetch(onSuccess, onError)
    }
}
