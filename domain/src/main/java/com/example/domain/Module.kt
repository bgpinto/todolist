package com.example.domain

import com.example.domain.repository.TodoItemRepository
import com.example.domain.sources.NetWorkDataSource
import com.example.domain.viewmodels.TodoItemListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val domainModule = module {
    single { TodoItemRepository(NetWorkDataSource()) }
    viewModel { TodoItemListViewModel(get()) }
}
