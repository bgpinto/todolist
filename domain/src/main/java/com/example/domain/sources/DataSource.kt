package com.example.domain.sources

import com.example.domain.models.TodoItemModel
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.gson.responseObject
import com.github.kittinunf.result.Result


interface DataSource {
    fun fetch(onSuccess: (List<TodoItemModel>) -> Unit, onError: (Throwable) -> Unit)
}


class NetWorkDataSource : DataSource {

    private val API_URL = "https://jsonplaceholder.typicode.com/todos"

    override fun fetch(onSuccess: (List<TodoItemModel>) -> Unit, onError: (Throwable) -> Unit) {
        Fuel.get(API_URL)
            .responseObject<List<TodoItemModel>> { request, response, result ->
                when (result) {
                    is Result.Success -> onSuccess(result.get())
                    is Result.Failure -> onError(result.error)
                }
            }
    }

}
